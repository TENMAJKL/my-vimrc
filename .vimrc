syntax enable 
set tabstop=4
set shiftwidth=4
set expandtab
set number
filetype indent on


set backspace=indent,eol,start

call plug#begin()
Plug 'vbe0201/vimdiscord'
Plug 'mattn/emmet-vim'
Plug 'codota/tabnine-vim'
Plug 'joshdick/onedark.vim'
Plug 'vim-airline/vim-airline'
call plug#end()

let g:user_emmet_leader_key=','


colorscheme onedark
